#include "functions.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <sys/time.h>

#include <cstdlib>
#include <fstream>
#include <time.h>
#include <sstream>


using namespace std;


void creatNonConscN(string w, int n,  int k, int * C, vector<string> &N)
{
	
	int f =0;
	for ( int i=0; i<=n-k; i++)
	{
		if(C[i] ==0)
		{
			N.push_back(w.substr(i,k));  
		}
	}

}

unsigned int New_regex(string w, int k, int * C, vector<string> &E){
	
	
	vector<string> N;
	int n = w.length();
	creatNonConscN(w,n,k,C, N);
	
	string tmp;
	string U;
	string V;
	string A;
	int flag=0;
	string add_pre="(";
	string add_suf="(#";
	string add="#(";
	for(int a=1; a<=k-1;a++)
	{
		add +="(a|b|())";
		add_pre +="(a|b|())";
		add_suf +="(a|b|())";
	}
	add +="#)*";
	add_pre +="#)*";
	add_suf +=")*";

	vector<string>::const_iterator it=N.begin();
	
	 if(N.size()==0 )
    {    
		E.push_back(add_pre);	
		E.push_back(add_suf);
		return 1;	
    }
    if(N.size()==1 )
    {    
		E.push_back(add_pre);
		E.push_back(*it);
		E.push_back(add_suf);	
		return 1;	
    }
    if(N.size()>=2)
    {
        while(it!=N.end()-1)
        {
            tmp=*it;
        
            if (flag ==0)
            {
				E.push_back(add_pre);
				E.push_back(tmp);
                flag=1;
            }

             U = tmp.substr(tmp.length()-k+1,k-1);
			 it++;

			tmp=*it;

            V = tmp.substr(0,k-1);

            if(V.compare(U)==0)
            {
                A = tmp.substr(k-1,tmp.length()-k+1);
				E.push_back("("+A+"|");
				E.push_back(add);
				E.push_back(tmp);
				E.push_back(")");
            }
            else{
				E.push_back(add);
				E.push_back(tmp);
            } 
        }
		E.push_back(add_suf);	
    }
	return 1;	
}





int main ( int argc, const char* argv[] )
{
	int k =  atoi(argv[3]); 
	char * W;
		
	FILE *ifile;  
	FILE *input_sen;

	char s[255];
	sprintf(s, "%s", argv[1]);
		 	
	ifile=fopen(s, "r");
	if(ifile==0){
			printf("File not found\n");
			return 0;
	}
					
	string myx;
				
	char c;
				
	do{
		c=fgetc(ifile);
		if(feof(ifile) || c=='\n')
				break;
		myx+=c;
	}while(1);
				
	W = new char[myx.length()+1];
	strcpy(W, myx.c_str());
		
	int n = strlen( W );

		
	int * C;
	C = (int*) calloc(n, sizeof(int));
		
	if (C ==NULL){		
		cout<<"Failed to allocate memory for C"<<endl;
	}
		
	char t[255];
	sprintf(t, "%s", argv[2]);
		 
	input_sen=fopen(t, "r");
	if(input_sen==0){
		printf("File not found\n");
		return 0;
	}
		
	int *b=(int*) calloc(n, sizeof(int));

	int num=0;
	fscanf(input_sen,"%d",&num);

	for(int j=0;j<=(num-1);j++){
		int ret=fscanf(input_sen,"%d",&b[j]);

		if(ret==EOF)
			break;
		C[b[j]]=1;
	}
 
    char * y = ( char * ) calloc ( k * n , sizeof(char));
      		
	vector<string> NEW_E;
	New_regex(W,k, C, NEW_E);
		
		
	string NEW_Es;
	for (vector<string>::const_iterator it=NEW_E.begin();it!=NEW_E.end();++it)
	{
		NEW_Es += *it;
	}
		
	cout<<"-----------------RESULTS---------------"<<endl;
	cout<<"W = "<<W<<endl;
	cout<<"k = "<<k<<endl;
	cout<<"Sensitive patterns position in W (starting from 0) with length "<<k<<endl ;
	for(int i=0;i<n;i++){
		if(C[i]==1)
		cout<<"C["<<i<<"] = "<<C[i]<<endl;
	}	
	cout<<"Regular Expression = "<<NEW_Es<<endl;
	
	free(C);
	free(y);
    free(W);

	return 1;

}
