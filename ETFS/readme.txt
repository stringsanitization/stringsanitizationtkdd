
COMPILATION AND EXAMPLE
------------------------
The implementation of our apporach that is used in experiments 
compiles and runs on a small example with ./compile.txt

Our approach compile with g++ -std=c++11 -O3 
We used g++ version (6.2.0) and it was tested on the following operating systems (MacOS Mojave 10.14.2, Scientific Linux 6.6)

INFORMATION ABOUT THE INPUT AND OUTPUT
---------------------------------------
Our approach 

  Input parameters (we refer to the parameters using the example in ./compile.txt):

    test.txt: This is the input string. It should be a single line.

    sen_pos_test.txt: These are the number and positions of sensitive patterns. The first line is the number of sensitive patterns, and the remaining lines are the 
                  starting positions of the sensitive patterns in the input string.  

    k: This is the parameter k (pattern length). All sensitive patterns must have length k


  The output is displayed on the screen and includes the input string W, the k, the starting positions of sensitive patterns, and the output of regular expression.
  For example, one possible output (recall that PFS-ALGO can produce more than one possible string Y) is the following:
-----------------RESULTS---------------
W = aaabbaabaccbbb
k = 4
Sensitive patterns position in W (starting from 0) with length 4
C[1] = 1
C[2] = 1
C[3] = 1
C[4] = 1
C[9] = 1
Regular Expression = ((a|b|())(a|b|())(a|b|())#)*aaab(a|#((a|b|())(a|b|())(a|b|())#)*aaba)(c|#((a|b|())(a|b|())(a|b|())#)*abac)(c|#((a|b|())(a|b|())(a|b|())#)*bacc)(b|#((a|b|())(a|b|())(a|b|())#)*accb)#((a|b|())(a|b|())(a|b|())#)*cbbb(#(a|b|())(a|b|())(a|b|()))*


The regular expression can be given as input to an algorithm that performs regular approximate matching, to produce
the sting X_{ED} directly. An implementation of such an algorithm is https://github.com/julianthome/prex


Comments and Questions
----------------------
Huiping Chen
huiping.chen@kcl.ac.uk

Grigorios Loukides
grigorios.loukides@kcl.ac.uk
