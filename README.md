# StringSanitizationTKDD

# What is this repository for? #
This repository contains the source code of the approach in 'Combinatorial Algorithms for String Sanitization' that appears in ACM Transactions on Knowledge Discovery from Data (TKDD).


# How do I get set up?#

There are three folders: TPM, TMI, and ETFS.
Each folder includes a readme file that explains how to execute the code.

# Who do I talk to? #

Huiping Chen huiping.chen@kcl.ac.uk
Grigorios Loukides grigorios.loukides@kcl.ac.uk