//
//  main.cpp
//
//  Created by ??? on 30/01/2019.
//  Copyright © 2019 ???. All rights reserved.
//

#include <math.h>
#include <stdlib.h>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <set>
#include <algorithm>
#include "functions.h"
#include "heuristic.h"
#include <ctime>



using namespace std;

int main(int argc, const char * argv[]) {

    if( argc!=12)
    {
        cout<<"   ./main text.txt sensitive.txt k tau cost_filename weight_filename theta sensitive_patterns_filename output_knapsack_filename  implausible_file rho"<<endl; 
        return -1;
    }

    FILE *ifile;  
    FILE *input_sen;  
    FILE *sensitive_patterns_filename; 
    FILE *output_knapsack_filename;
    FILE *implausible_file;   
    int n;
    int k=atoi(argv[3]); 
    int tau=atoi(argv[4]);

    int * C;
    char * y;
    char c;

    string cost_filename(argv[5]);
    string weight_filename(argv[6]);
    int theta=-1*atoi(argv[7]);
    string out_knap_filename(argv[9]);

    bool implausible_handling=false;
    double rho=0.0;

    if(argc==12)
    {
        implausible_handling=true;
        rho=atof(argv[11]);
    }


    set<string> sensitive_patterns;

    sensitive_patterns_filename=fopen(argv[8], "r");

    if(sensitive_patterns_filename==NULL)
    {
        printf("File sens patterns not found\n");
        return 0;
    }

    char* sp_line;
    size_t len=80;

    sp_line = (char *)malloc(len * sizeof(char));
    while((getline(&sp_line, &len, sensitive_patterns_filename))!=-1)
    {
        string sp_line_str(sp_line);
        if(sp_line_str.length()>1)
            sensitive_patterns.insert(sp_line_str.substr(0,sp_line_str.length()-1));
    }
    
    fclose(sensitive_patterns_filename);
    if(sp_line)
        free(sp_line);

    cout<<"-----------------RESULTS---------------"<<endl;
    cout<<"Sensitive patterns:\n";
    for(auto& it : sensitive_patterns)
        cout<<"["<<it<<"]\n";
    string myx;

    char s[255];
    sprintf(s, "%s", argv[1]);
    ifile=fopen(s, "r");
    if (ifile==0) {
        printf("File not found\n");
        return 0;
    }

    int i=0;
    do{
        c=fgetc(ifile);
        if(feof(ifile) || c=='\n')
            break;
        myx+=c;
        }while(1);
    
    char *x=new char[myx.length()+1];
    strcpy(x, myx.c_str());



    n = strlen(x)-1;


    C = (int*) calloc(n, sizeof(int));
    y = (char*) calloc(ceil(double(n-k+1)/2)*k+floor((double)(n-k+1)/2), sizeof(char));

    if(C==NULL)
    {
        cout<<"Failed to allocate memory for y\n";
        return -1;

    }
    if(y==NULL)
    {
        cout<<"Failed to allocate memory for y\n";
        return -1;
    }

    char t[255];
    sprintf(t, "%s", argv[2]);

    input_sen=fopen(t, "r");
    if (input_sen==0) {
        printf("File not found\n");
        return 0;
    }


    int *b=(int*) calloc(n, sizeof(int));


    int num=0;
    fscanf(input_sen,"%d",&num);

    for(int j=0;j<=(num-1);j++){
        int ret=fscanf(input_sen,"%d",&b[j]);

        if(ret==EOF)break;

        C[b[j]]=1;
    }

    if(fclose(ifile))
        printf("File close error.\n");
    if(fclose(input_sen))
        printf("File close error.\n");

    for (i = n-1; i >= n - k + 1; i--)
    {
        C[i] = C[n-k];

    }


    int l = sut( x, C, k, '#', y);
	

    string y_temp(y);
    string y_4="";

    string y_2(y_temp.substr(0,y_temp.length()-1));
    y_4 += y_2;


    char *y3=const_cast<char *> (y_2.c_str()) ;

	int a=0;
	
    
	vector<string> Z;
	string Zs;

	Zs = y_4;

	int delta=0;
	for(string::const_iterator it=Zs.begin();it!=Zs.end();++it)
	{
		if(*it=='#')delta++;
	}

    free (C);
    free (y);


    unordered_map<string, int> H;

    unordered_set<string> deleted_from_H;

    add_to_H_frequent_in_Z(Zs, H, k, tau, deleted_from_H);



    set<char> distinct_in_Zs(Zs.begin(),Zs.end());

    distinct_in_Zs.insert((char)0);


    set<char>::iterator it=distinct_in_Zs.find('#');
    if(it!=distinct_in_Zs.end())
        distinct_in_Zs.erase(it);

 unordered_set<string> implausible_patterns;
   if(implausible_handling)
  {

  	  if(delta>0)
  	 {    

  	 	 int lambda=k;      
  	 	 string W=string(x,n);
	 
  	 	 find_implausible_patterns(Zs, W, rho, lambda, distinct_in_Zs, implausible_patterns);

  	  }

  	   implausible_file=fopen(argv[10],"w");

       string implausible_filename(argv[10]);

	        if(implausible_file==NULL)
	        {
	            printf("Error opening file for writing: %s\n",implausible_filename.c_str());
            }
           

        fclose(implausible_file);

  }


    update_H_with_new_in_Zprime(Zs, H, k, tau, distinct_in_Zs, deleted_from_H);

    string Z_prime;

    vector<std::pair<int,int> > knapsack_symbols;

    set<int> hash_replaced_by_empty;
    vector<char> selected_chars;

    if(delta>0)
    {
		if(implausible_patterns.size()==0)
			cout<<"Implausible patterns is empty "<<endl;
		else
			cout<<"Implausible patterns: "<<endl;

		if(implausible_handling)
		{
			for(auto &it: implausible_patterns)
			{
				sensitive_patterns.insert(it);
				cout<<"["<<it<<"]"<<endl;
			}
		}
	
	
        assign_ghost_and_sub(Zs, H, k, tau, distinct_in_Zs, sensitive_patterns, theta, cost_filename, weight_filename);

        string tmp="./mcknap_clean "+to_string(delta)+" "+to_string(distinct_in_Zs.size())+" "+to_string(1)+" "+to_string(theta)+" "+cost_filename+" "+weight_filename+" "+to_string(distinct_in_Zs.size())+" "+to_string(delta)+" "+out_knap_filename;
        system(tmp.c_str());



        output_knapsack_filename=fopen(argv[9], "r");


        if(output_knapsack_filename==NULL)
        {
            printf("Knapsack output file not found. This happens when knapsack is unsolvable or has trivial solution and we need to change theta\n");
            return 0;
        }

        char ch;
        if(fscanf(output_knapsack_filename,"%c",&ch)==EOF)
        {
            printf("File is Empty. Knapsack is trivial or unsolvable, please choose a different C.\n\n");
            fclose(output_knapsack_filename);
            return 0;
        }
        fclose(output_knapsack_filename);
        output_knapsack_filename=fopen(argv[9], "r");

       

        char* oknap_line;
        size_t len2=80;
        oknap_line = (char *)malloc(len2 * sizeof(char));
        while((getline(&oknap_line, &len2, output_knapsack_filename))!=-1)
        {
            string str_oknap_line(oknap_line);

            if(str_oknap_line.length()>1)
            {
                char *hash_id=strtok(oknap_line, " ");
                char *replacement_for_hash=strtok(NULL, " ");

                int hash_id_int=atoi(hash_id);
                int replacement_for_hash_int=atoi(replacement_for_hash);

                knapsack_symbols.push_back(make_pair(hash_id_int, replacement_for_hash_int));
            }
            else
            {
                cout<<"Error in knapsack output file. Each line must have two integers\n";
                return 0;
            }
        }
        fclose(output_knapsack_filename);
        if(oknap_line)
            free(oknap_line);

		sort(knapsack_symbols.begin(), knapsack_symbols.end());



        vector<char> distinct_in_Zs_vec(distinct_in_Zs.begin(),distinct_in_Zs.end());
        int hash_no=0;
        for (string::const_iterator it=Zs.begin();it!=Zs.end();++it)
        {
            if(*it=='#')
            {

                pair<int, int> p=knapsack_symbols.at(hash_no);
                char selected_char=distinct_in_Zs_vec.at(p.second);
                Z_prime+=selected_char;

                hash_no++;

            }
            else
                Z_prime+=*it;
        }
    }
    else
    {
        Z_prime=Zs;
    }

	cout<<"W = "<<myx<<endl;
	cout<<"X = "<<y_4<<endl;
	cout<<"Z = "<<Z_prime<<endl;


    return 0;

}
