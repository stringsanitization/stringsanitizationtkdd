#ifndef heuristic
#define heuristic

#include <stdio.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <set>

using namespace std;

void add_to_H_frequent_in_Z(string Zs, unordered_map<string, int> &H, int k,int tau, unordered_set<string>& deleted_from_H);
void update_H_with_new_in_Zprime(string Zs, unordered_map<string, int> &H, int k,int tau, set<char>& distinct_in_Zs, unordered_set<string>& deleted_from_H);
void assign_ghost_and_sub(string Zs, unordered_map<string, int> &H, int k,int tau, set<char>& distinct_in_Zs, set<string>& sensitive_patterns, int theta, string cost_filename, string weight_filename);

void find_implausible_patterns(string Y, string W, double rho, int lambda, set<char> &distinct_Zs, unordered_set<string>& implausible_patterns); 
void count_implausible_patterns_in_Z(string Y,string Z, vector<char> selected_chars, set<int>& hash_replaced_by_empty, unordered_set<string>& implausible_patterns, double rho, int k);

void measure_side_effects(string& X, string& X_prime, int& no_of_ghosts, int& no_of_losts, int k, int tau, set<string>& sensitive_patterns, string input_filename, int no_of_sensitive_positions);

#endif

