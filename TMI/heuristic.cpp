#include "heuristic.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <cstdio>
#include <string>
#include <sstream>
#include <set>
#include <fstream>
#include <climits>
#include <math.h>
#include <unordered_set>
#include <algorithm>
#include <string.h>

using namespace std;



#include<iostream> 
using namespace std; 


void computeLPSArray(const char* pat,  int M, int* lps) ;


int KMPSearch(const char* pat, const char* txt) 
{ 
    int M = strlen(pat); 
    int N = strlen(txt); 
	int cnt=0;
   
    int lps[M]; 

    computeLPSArray(pat, M, lps); 
  
    int i = 0; 
    int j = 0; 
    while (i < N) { 
        if (pat[j] == txt[i]) { 
            j++; 
            i++; 
        } 
  
        if (j == M) { 
			cnt++;

            j = lps[j - 1]; 
        } 
  
        
        else if (i < N && pat[j] != txt[i]) { 
   
            if (j != 0) 
                j = lps[j - 1]; 
            else
                i = i + 1; 
        } 
    } 
	return cnt;
} 
  

void computeLPSArray(const char* pat,  int M, int* lps) 
{ 

    int len = 0; 
	lps[0] = 0; 
  

    int i = 1; 
    while (i < M) { 
        if (pat[i] == pat[len]) { 
            len++; 
            lps[i] = len; 
            i++; 
        } 
        else 
        { 
            if (len != 0) 
                len = lps[len - 1]; 

            else 
            { 
                lps[i] = 0; 
                i++; 
            } 
        } 
    } 
} 


void count_implausible_patterns_in_Z(string Y,string Z, vector<char> selected_chars, set<int>& hash_replaced_by_empty, unordered_set<string>& implausible_patterns, double rho, int k)
{

	int pos=0;
	int cnt_hashes=0;

    
    unordered_set<string> unprotected_implausible;

    for(string::const_iterator it=Y.begin();it!=Y.end();++it)
    {           
		 string new_part;	
		 if(*it=='#')
		{
			set<int>::const_iterator found=hash_replaced_by_empty.find(cnt_hashes);

			if(found!=hash_replaced_by_empty.end())	
			{
				new_part=Y.substr(pos-(k-1),k-1)+Y.substr(pos+1,k-1);	
			
			}
			else
			{
				string ch;
				ch.push_back(selected_chars.at(cnt_hashes));
				new_part=Y.substr(pos-(k-1),k-1)+ch+Y.substr(pos+1,k-1);
			}

			cnt_hashes++;
		}
		
	

		for(auto it : implausible_patterns)
		{
			std::size_t  found=new_part.find(it);
			if (found!=std::string::npos)
				unprotected_implausible.insert(it);
		}
		
		pos++;
    }	
    cout<<unprotected_implausible.size()<<" out of "<<implausible_patterns.size()<<" implausible found in Z around #s "<<double(unprotected_implausible.size())/implausible_patterns.size()*100<<"\%"<<endl;
}

void find_implausible_patterns(string Y, string W, double rho, int lambda, set<char> &distinct_Zs, unordered_set<string>& implausible_patterns) 
{
   
    unordered_set<string> candidate_substrings;

    bool seen_multiple_hashes=false;


    int pos=0;
    for(string::const_iterator it=Y.begin();it!=Y.end();++it)
    {   
        
	 if(*it=='#')
	{
	    for(set<char>::iterator it2=distinct_Zs.begin();it2!=distinct_Zs.end();++it2)               
            {
                string new_part;
                if(*it2==(char)0)
                    new_part=Y.substr(pos-(lambda-1),lambda-1)+Y.substr(pos+1,lambda-1);				
                else
                    new_part=Y.substr(pos-(lambda-1),lambda-1)+*it2+Y.substr(pos+1,lambda-1);
                
                int j=0;
                            
                                
                while(j+lambda<=new_part.length())
                {
                    string klen=new_part.substr(j,lambda);
		  
				candidate_substrings.insert(klen);

				j++;
		}
	      }
	}
      pos++;
    }

    for(auto it : candidate_substrings)	
    {

		string prefix=it.substr(0,it.size()-1);
        string suffix=it.substr(1,it.size()-1);
		string infix=it.substr(1,it.size()-2);
	
		const char *charW = W.c_str();
		const char *pref = prefix.c_str();
		const char *suf = suffix.c_str();
		const char *inf = infix.c_str();
		const char *charit = it.c_str();
	
	  
		int freq_prefix=KMPSearch(pref,charW);
		int freq_suffix=KMPSearch(suf,charW);
		int freq_infix=KMPSearch(inf,charW);
		int freq_word=KMPSearch(charit,charW);

		double E=0.0;
		if(freq_infix>0)
		{
			E=((double)(freq_prefix*freq_suffix))/freq_infix;
		}
		
		
		double z=((double)(freq_word-E))/max(sqrt(E),1.0);
	

		if(z<rho)
			implausible_patterns.insert(it);
    }
  
}
void assign_ghost_and_sub(string Zs, unordered_map<string, int> &H, int k, int tau, set<char>& distinct_in_Zs, set<string>& sensitive_patterns, int theta, string cost_filename, string weight_filename)
{
    
    vector<vector<int> > costs_for_all_hashes;
    vector<vector<int> > weights_for_all_hashes;
    
    int pos=0;
    for(string::const_iterator it=Zs.begin();it!=Zs.end();++it)
    {
        if(*it=='#')
        {
            
            vector<int> costs;
            vector<int> weights;
            
            for(set<char>::iterator it2=distinct_in_Zs.begin();it2!=distinct_in_Zs.end();++it2)               
            {
                string new_part;
                if(*it2==(char)0)
                    new_part=Zs.substr(pos-(k-1),k-1)+Zs.substr(pos+1,k-1);				
                else
                    new_part=Zs.substr(pos-(k-1),k-1)+*it2+Zs.substr(pos+1,k-1);
                
                int j=0;
                
                int ghosts=0;
                
                
                bool creates_sens=false;
                while(j+k<=new_part.length())
                {
                    string klen=new_part.substr(j,k);
                    
                    unordered_map<string,int>::const_iterator h_it=H.find(klen);
                    if(h_it!=H.end())
                    {
                        set<string>::const_iterator it3=sensitive_patterns.find(h_it->first);
                        
                        
                        if(it3==sensitive_patterns.end())
                            ghosts++;

                        else 
                            creates_sens=true;
        
                    }			
                    else 
                    {
                        set<string>::const_iterator it3=sensitive_patterns.find(klen);
                        
                        if(it3!=sensitive_patterns.end())
                            creates_sens=true;
                        
                    }
                    j++;
                }
                
                if(creates_sens==false)
                {
                    costs.push_back(-1*ghosts); 
                    weights.push_back(-1); 
                }
                else		
                {
                    costs.push_back(0);
                    weights.push_back(30000); 
                }

            }
            costs_for_all_hashes.push_back(costs);
            weights_for_all_hashes.push_back(weights);
            
        }
        
        pos++;
    }
    
    
    ofstream cfile (cost_filename.c_str(), std::ofstream::out);
    
    for(vector<vector<int> >::const_iterator it=costs_for_all_hashes.begin();it!=costs_for_all_hashes.end();++it)
    {
        vector<int> cur=*it;
        
        for(auto& it : cur)
        {
            cfile<<it<<" ";
        }	
        cfile<<"\n";
    }
    cfile.close();
    
    
    ofstream wfile (weight_filename.c_str(), std::ofstream::out);
    
    for(vector<vector<int> >::const_iterator it=weights_for_all_hashes.begin();it!=weights_for_all_hashes.end();++it)
    {
        vector<int> cur=*it;
        for(auto& it : cur)
        {
            wfile<<it<<" ";
        }	
        wfile<<"\n";
    }
    wfile.close();
    
}

void update_H_with_new_in_Zprime(string Zs, unordered_map<string, int> &H, int k, int tau, set<char>& distinct_in_Zs,  unordered_set<string>& deleted_from_H)
{
    
    int pos=0;
    
    
    for(string::const_iterator it=Zs.begin();it!=Zs.end();++it)
    {
        if(*it=='#')  
        {
            
            unordered_map<string, int> H_for_hash;
            
            int cnt=0; 
            
            for(set<char>::iterator it2=distinct_in_Zs.begin();it2!=distinct_in_Zs.end();++it2)
            {
                
                
                unordered_map<string, int> H_for_hash_tmp;
                
                string new_part;
                
                if(*it2==(char)0)
                    new_part=Zs.substr(pos-(k-1),k-1)+Zs.substr(pos+1,k-1);
                else
                    new_part=Zs.substr(pos-(k-1),k-1)+*it2+Zs.substr(pos+1,k-1);
                
                
                int j=0;
                while(j+k<=new_part.length())
                {
                    string klen=new_part.substr(j,k);
                    unordered_map<string,int>::const_iterator h_it=H_for_hash_tmp.find(klen);
                    
                    if(h_it==H_for_hash_tmp.end())
                        H_for_hash_tmp[klen]=1;
                    else	 
                        H_for_hash_tmp[klen]++;
                    
                    j++;
                }
                if(cnt==0) 
                {
                    H_for_hash=H_for_hash_tmp; 				
                }
                else
                {
                    for(auto &it5 : H_for_hash_tmp)
                    {
                        unordered_map<string, int>::const_iterator it6=H_for_hash.find(it5.first);
                        
                        if(it6==H_for_hash.end())
                            H_for_hash[it5.first]=it5.second;
                        else
                        {
                            if(it6->second > it5.second)
                                H_for_hash[it6->first]=it5.second;
                        
                        }
                    }
                    
                }
                cnt++;
            }
            
            for(auto &it5 : H_for_hash)
            {
                unordered_map<string, int>::const_iterator it6=H.find(it5.first);
                if(it6==H.end())
                    H[it5.first]=it5.second;
                else
                    H[it6->first]=it6->second+it5.second; 
                    
            }		
        }
        
        pos++;
    }
    
    for(unordered_map<string,int>::iterator it=H.begin();it!=H.end();)
    {
        unordered_set<string>::iterator it2=deleted_from_H.find(it->first);
        
        if(it->second < tau || it2!=deleted_from_H.end()) 
            it=H.erase(it);	
        
        else
            ++it;
    }
    
}

void add_to_H_frequent_in_Z(string Zs, unordered_map<string, int> &H, int k, int tau, unordered_set<string>& deleted_from_H)
{
    
    vector <string> tokens; 
    
    stringstream check1(Zs); 
    
    string token; 
    
    while(getline(check1, token, '#')) 
    { 
        tokens.push_back(token); 
    } 
    
    for(vector<string>::iterator it=tokens.begin();it!=tokens.end();++it)
    {
        string cur=*it;
        
        int j=0;
        while(j+k<=cur.length())
        {
            string klen=cur.substr(j,k);
            if(klen.length()<k)
                continue;
            
            unordered_map<string,int>::const_iterator h_it=H.find(klen);
            
            if(h_it==H.end())
                H[klen]=1;
            else	 
                H[klen]++;
            
            j++;
        }
        
    }
    
    
    for(unordered_map<string,int>::iterator it=H.begin();it!=H.end();)
    {
        
        if(it->second>=tau)
        {
            deleted_from_H.insert(it->first);
            it=H.erase(it);	
        }
        else
            ++it;
    }
    
}
